﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebsiteBanHang.Models;
using WebsiteBanHang.Repositories;

namespace WebsiteBanHang.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = SD.Role_Admin)]
    public class BrandManagerController : Controller
    {
        private readonly IBrandRepository _brandRepository;
        public BrandManagerController(IBrandRepository brandRepository)
        {
            _brandRepository = brandRepository;
        }
        // Hiển thị danh sách sản phẩm
        public async Task<IActionResult> Index()
        {
            var brands = await _brandRepository.GetAllAsync();
            return View(brands);
        }
        // Hiển thị form thêm sản phẩm mới
        public async Task<IActionResult> Create()
        {
            var brands = await _brandRepository.GetAllAsync();
            return View();
        }
        // Xử lý thêm sản phẩm mới
        [HttpPost]
        public async Task<IActionResult> Create(Brand brand)
        {
            if (ModelState.IsValid)
            {
                await _brandRepository.AddAsync(brand);
                return RedirectToAction(nameof(Index));
            }
            // Nếu ModelState không hợp lệ, hiển thị form với dữ liệu đã nhập
            var brands = await _brandRepository.GetAllAsync();
            return View(brand);
        }
        // Viết thêm hàm SaveImage (tham khảo bào 02)
        // Hiển thị form cập nhật sản phẩm
        public async Task<IActionResult> Edit(int id)
        {
            var brand = await _brandRepository.GetByIdAsync(id);
            if (brand == null)
            {
                return NotFound();
            }
            var brands = await _brandRepository.GetAllAsync();
            return View(brand);
        }
        // Xử lý cập nhật sản phẩm
        [HttpPost]
        public async Task<IActionResult> Edit(int id, Brand brand)
        {
            ModelState.Remove("ImageUrl"); // Loại bỏ xác thực ModelState cho ImageUrl
            if (id != brand.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                var existingBrand = await
                _brandRepository.GetByIdAsync(id); // Giả định có phương thức GetByIdAsync
                                                   // Giữ nguyên thông tin hình ảnh nếu không có hình mới đượctải lên              
                                                   // Cập nhật các thông tin khác của sản phẩm
                existingBrand.Id = brand.Id;
                existingBrand.Name = brand.Name;
                await _brandRepository.UpdateAsync(existingBrand);
                return RedirectToAction(nameof(Index));
            }
            var brands = await _brandRepository.GetAllAsync();
            ViewBag.Categories = new SelectList(brands, "Id", "Name");
            return View(brand);
        }
        // Hiển thị form xác nhận xóa sản phẩm
        public async Task<IActionResult> Delete(int id)
        {
            var brand = await _brandRepository.GetByIdAsync(id);
            if (brand == null)
            {
                return NotFound();
            }
            return View(brand);
        }
        // Xử lý xóa sản phẩm
        [HttpPost]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var brand = await _brandRepository.GetByIdAsync(id);
            if (brand == null)
            {
                return NotFound();
            }
            await _brandRepository.DeleteAsync(id);
            return Json(new { success = true });
        }

    }
}
