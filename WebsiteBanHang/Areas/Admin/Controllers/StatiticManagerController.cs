﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebsiteBanHang.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace WebsiteBanHang.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = SD.Role_Admin)]
    public class StatiticManagerController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public StatiticManagerController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index(DateTime? startDate, DateTime? endDate)
        {
            if (!endDate.HasValue)
            {
                endDate = DateTime.Today;
            }

            ViewBag.StartDate = startDate?.ToString("yyyy-MM-dd");
            ViewBag.EndDate = endDate?.ToString("yyyy-MM-dd");

            var ordersQuery = _context.Orders
                .Include(o => o.OrderDetails)
                .ThenInclude(od => od.Product)
                .AsQueryable();

            if (startDate.HasValue)
            {
                ordersQuery = ordersQuery.Where(o => o.OrderDate >= startDate.Value);
            }

            ordersQuery = ordersQuery.Where(o => o.OrderDate <= endDate.Value);

            var orders = await ordersQuery.ToListAsync();

            var totalOrders = orders.Count;
            var totalRevenue = orders.Sum(o => o.OrderDetails.Sum(od => od.Price * od.Quantity));

            ViewData["TotalOrders"] = totalOrders;
            ViewData["TotalRevenue"] = totalRevenue.ToString("N0");

            var orderWithEmails = orders.Select(async order => new OrderWithEmail
            {
                Order = order,
                Email = (await _userManager.FindByIdAsync(order.UserId)).Email,
                FullName = (await _userManager.FindByIdAsync(order.UserId)).FullName
            }).Select(t => t.Result).ToList();

            return View(orderWithEmails);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteOrder(int orderId)
        {
            var order = await _context.Orders.FindAsync(orderId);
            if (order != null)
            {
                _context.Orders.Remove(order);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> OrderDetails(int id)
        {
            var order = await _context.Orders
                .Include(o => o.OrderDetails)
                .ThenInclude(od => od.Product)
                .FirstOrDefaultAsync(o => o.Id == id);

            if (order == null)
            {
                return NotFound();
            }

            var user = await _userManager.FindByIdAsync(order.UserId);
            ViewBag.CustomerName = user.FullName;

            return View(order);
        }

        public class OrderWithEmail
        {
            public Order Order { get; set; }
            public string Email { get; set; }
            public string FullName { get; set; }
        }
    }
}
