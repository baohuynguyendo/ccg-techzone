﻿namespace WebsiteBanHang.Models
{
    public class HomeViewModel
    {
        public IEnumerable<Product> Product { get; set; }
        public IEnumerable<Category> Categories { get; set; }
    }
}
