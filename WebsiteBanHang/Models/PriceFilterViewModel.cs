﻿namespace WebsiteBanHang.ViewModels
{
    public class PriceFilterViewModel
    {
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
    }
}