namespace WebsiteBanHang.Models;

public class OrderInfoModel
{
    public int Id { get; set; }
    public string FullName { get; set; }
    public string OrderId { get; set; }
    public string OrderInfo { get; set; }
    public double Amount { get; set; }
}