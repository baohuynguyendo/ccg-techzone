﻿// Banner
const bannerController = {
    prevArrow: null,
    nextArrow: null,
    bannerList: null,
    dots: null,
    currentIndex: 0,
    bannerCount: 0,

    init: function () {
        this.prevArrow = document.querySelector('.prev-arrow');
        this.nextArrow = document.querySelector('.next-arrow');
        this.bannerList = document.querySelector('.banner-list');
        this.dots = document.querySelectorAll('.dot');
        this.bannerCount = this.bannerList.children.length;

        this.prevArrow.addEventListener('click', () => this.prevBanner());
        this.nextArrow.addEventListener('click', () => this.nextBanner());
        this.dots.forEach((dot, index) => {
            dot.addEventListener('click', () => this.showBanner(index));
        });
    },

    prevBanner: function () {
        this.currentIndex = (this.currentIndex - 1 + this.bannerCount) % this.bannerCount;
        this.updateBanner();
    },

    nextBanner: function () {
        this.currentIndex = (this.currentIndex + 1) % this.bannerCount;
        this.updateBanner();
    },

    showBanner: function (index) {
        this.currentIndex = index;
        this.updateBanner();
        this.updateDots();
    },

    updateBanner: function () {
        const translateX = -this.currentIndex * 100;
        this.bannerList.style.transform = `translateX(${translateX}%)`;
        this.updateDots();
    },

    updateDots: function () {
        this.dots.forEach((dot, index) => {
            if (index === this.currentIndex) {
                dot.classList.add('active');
            } else {
                dot.classList.remove('active');
            }
        });
    }
};

// Lưu trữ vị trí cuộn trang
const scrollController = {
    init: function () {
        window.addEventListener('scroll', () => this.saveScrollPosition());
        window.onload = () => this.loadScrollPosition();
    },

    saveScrollPosition: function () {
        sessionStorage.setItem('scrollPosition', window.pageYOffset);
    },

    loadScrollPosition: function () {
        const savedScrollPosition = sessionStorage.getItem('scrollPosition');
        if (savedScrollPosition) {
            window.scrollTo(0, savedScrollPosition);
        }
    }
};

// Hiển thị tên sản phẩm
const productNameController = {
    init: function () {
        document.addEventListener('DOMContentLoaded', () => this.truncateProductNames());
    },

    truncateProductNames: function () {
        const cardTitles = document.querySelectorAll('.card-title');
        cardTitles.forEach(function (title) {
            const productName = title.textContent.trim();
            if (productName.length > 35) {
                title.textContent = productName.substring(0, 35) + '...';
                title.setAttribute('title', productName);
            }
        });
    }
};

// Khởi tạo các controller
bannerController.init();
scrollController.init();
productNameController.init();