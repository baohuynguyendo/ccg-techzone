using WebsiteBanHang.Models;
using WebsiteBanHang.Models.MOMO;

namespace WebsiteBanHang.Services;

public interface IMomoService
{
    Task<MomoCreatePaymentResponseModel> CreatePaymentAsync(OrderInfoModel model);
    MomoExecuteResponseModel PaymentExecuteAsync(IQueryCollection collection);
}