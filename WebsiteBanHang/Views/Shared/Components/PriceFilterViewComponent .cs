﻿using Microsoft.AspNetCore.Mvc;
using WebsiteBanHang.ViewModels;

namespace WebsiteBanHang.Components
{
    public class PriceFilterViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            var model = new PriceFilterViewModel();
            return View(model);
        }
    }
}