﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using WebsiteBanHang.Models;
using WebsiteBanHang.Repositories;

namespace WebsiteBanHang.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IProductRepository _productRepository;          
        private readonly ILogger<HomeController> _logger;
        private readonly IBrandRepository _brandRepository;
        private readonly ICategoryRepository _categoryRepository;
        
        public HomeController(ILogger<HomeController> logger, IProductRepository productRepository, ApplicationDbContext context, IBrandRepository brandRepository, ICategoryRepository categoryRepository)
        {
            _logger = logger;
            _productRepository = productRepository;
            _context = context;
            _brandRepository = brandRepository;
            _categoryRepository = categoryRepository;
        }

        //public IActionResult Index()
        //{
        //    return View();
        //}
        public async Task<IActionResult> Index()
        {          
            var products = await _productRepository.GetAllAsync();
            var categories = await _context.Categories.ToListAsync();
            var bestSellers = await _context.Products
                                        .Where(p => p.Quantity > 50)
                                        .OrderByDescending(p => p.Quantity)
                                        .ToListAsync();

            // Truyền danh sách danh mục vào ViewBag
            ViewData["BestSellers"] = bestSellers;
            ViewBag.Categories = categories;          
            return View(products);
            
        }

        public async Task<IActionResult> Detail(int id)
        {
            var product = await _productRepository.GetByIdAsync(id);
            var brands = await _brandRepository.GetAllAsync();
            ViewBag.Brands = brands;
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }

        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Contact(Contact contact)
        {
            if (ModelState.IsValid)
            {
                // Xử lý dữ liệu liên hệ
                // Ví dụ: Lưu vào cơ sở dữ liệu hoặc gửi email
                _context.Contacts.Add(contact);
                await _context.SaveChangesAsync();               
                // Xóa trống form sau khi gửi thành công
                ModelState.Clear();              
            }
            
            return View(contact);
        }

        public async Task<IActionResult> FilterByCategory(int categoryId)
        {
            var categories = await _categoryRepository.GetAllAsync();
            ViewBag.Categories = categories;
            var bestSellers = await _context.Products
                                        .Where(p => p.Quantity > 50)
                                        .OrderByDescending(p => p.Quantity)
                                        .ToListAsync();

            // Truyền danh sách danh mục vào ViewBag
            ViewData["BestSellers"] = bestSellers;
            // Lấy danh sách sản phẩm theo categoryId từ cơ sở dữ liệu
            var products = _context.Products.Where(p => p.CategoryId == categoryId).ToList();
            // Truyền danh sách sản phẩm đến View
            return View("Index", products);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    
        [HttpGet]
        public IActionResult Search(string searchString)
        {
            if (string.IsNullOrEmpty(searchString))
            {
                // Nếu không có chuỗi tìm kiếm, trả về danh sách rỗng
                return View("SearchResult", new List<Product>());
            }

            // Lọc sản phẩm theo tên chứa searchString
            var filteredProducts = _context.Products
                .Where(p => p.Name.Contains(searchString))
                .ToList();

            // Trả về view SearchResult với danh sách sản phẩm đã lọc
            return View("SearchResult", filteredProducts);
        }
     
        [HttpGet]
        public IActionResult SearchSuggestions(string searchString)
        {
            var suggestions = _context.Products
                .Where(p => p.Name.Contains(searchString))
                .Select(p => new
                {
                    Id = p.Id,
                    Name = p.Name,
                    ImageUrl = p.ImageUrl,
                    Price = p.Price
                })
                .ToList();

            return Json(suggestions);
        }

        public async Task<IActionResult> FilterByPrice(decimal? minPrice, decimal? maxPrice)
        {
            var products = await _productRepository.GetAllAsync();

            if (minPrice.HasValue)
            {
                products = products.Where(p => p.Price >= minPrice.Value);
            }

            if (maxPrice.HasValue)
            {
                products = products.Where(p => p.Price <= maxPrice.Value);
            }

            var categories = await _categoryRepository.GetAllAsync();
            ViewBag.Categories = categories;

            var bestSellers = await _context.Products
                .Where(p => p.Quantity > 50)
                .OrderByDescending(p => p.Quantity)
                .ToListAsync();

            ViewData["BestSellers"] = bestSellers;

            return View("Index", products);
        }
    }

}

