﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebsiteBanHang.Extensions;
using WebsiteBanHang.Models;
using WebsiteBanHang.Repositories;
using WebsiteBanHang.Services;

namespace WebsiteBanHang.Controllers
{
    [Authorize]
    public class ShoppingCartController : Controller
    {
        private readonly IMomoService _momoService;
        private readonly IProductRepository _productRepository;
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public ShoppingCartController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IProductRepository productRepository, IMomoService momoService)
        {
            _context = context;
            _userManager = userManager;
            _productRepository = productRepository;
            _momoService = momoService;
        }

        public IActionResult Index()
        {
            var cart = HttpContext.Session.GetObjectFromJson<ShoppingCart>("Cart") ?? new ShoppingCart();
            return View(cart);
        }

        public async Task<IActionResult> AddToCart(int productId, int quantity)
        {
            var product = await GetProductFromDatabase(productId);

            if (product.Quantity == 0)
            {
                TempData["ProductOutOfStock"] = true;
                return RedirectToAction("Index", "Home", new { scrollPosition = 0 });
            }

            var cartItem = new CartItem
            {
                ProductId = productId,
                Name = product.Name,
                Image = product.ImageUrl,
                Price = product.Price,
                Quantity = quantity
            };

            var cart = HttpContext.Session.GetObjectFromJson<ShoppingCart>("Cart") ?? new ShoppingCart();
            cart.AddItem(cartItem);
            HttpContext.Session.SetObjectAsJson("Cart", cart);

            TempData["ProductAddedToCart"] = true;

            return RedirectToAction("Index", "Home", new { scrollPosition = 0 });
        }

        [HttpPost]
        public async Task<IActionResult> UpdateCartItem(Dictionary<int, int> quantities)
        {
            var cart = HttpContext.Session.GetObjectFromJson<ShoppingCart>("Cart");
            if (cart != null)
            {
                foreach (var item in cart.Items)
                {
                    if (quantities.TryGetValue(item.ProductId, out int quantity))
                    {
                        var product = await GetProductFromDatabase(item.ProductId);
                        if (quantity > product.Quantity)
                        {
                            item.Quantity = product.Quantity;
                            TempData["QuantityExceedsStock"] = product.Quantity;
                        }
                        else
                        {
                            item.Quantity = quantity;
                        }
                    }
                }

                HttpContext.Session.SetObjectAsJson("Cart", cart);
            }

            return RedirectToAction("Index");
        }

        public IActionResult RemoveFromCart(int productId)
        {
            var cart = HttpContext.Session.GetObjectFromJson<ShoppingCart>("Cart");
            if (cart is not null)
            {
                cart.RemoveItem(productId);
                HttpContext.Session.SetObjectAsJson("Cart", cart);
            }
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> CheckoutAsync()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Checkout(Order order)
        {
            var cart = HttpContext.Session.GetObjectFromJson<ShoppingCart>("Cart");
            if (cart == null || !cart.Items.Any())
            {
                return RedirectToAction("Index");
            }

            var user = await _userManager.GetUserAsync(User);
            order.UserId = user.Id;
            order.OrderDate = DateTime.UtcNow;
            order.TotalPrice = cart.Items.Sum(i => i.Price * i.Quantity);
            order.OrderDetails = cart.Items.Select(i => new OrderDetail
            {
                ProductId = i.ProductId,
                Quantity = i.Quantity,
                Price = i.Price
            }).ToList();

            _context.Orders.Add(order);
            await _context.SaveChangesAsync();
            HttpContext.Session.Remove("Cart");

            // Đọc nội dung từ file HTML
            var emailTemplate = System.IO.File.ReadAllText("C:\\Users\\acer\\Downloads\\DACS\\WebLKMT\\WebsiteBanHang\\WebsiteBanHang\\Views\\Email\\Email.cshtml");

            // Tạo nội dung chi tiết đơn hàng
            var orderDetailsHtml = "";
            foreach (var item in cart.Items)
            {
                // Đảm bảo URL hình ảnh là tuyệt đối
                var imageUrl = Url.Action("GetImage", "ShoppingCart", new { filename = item.Image }, Request.Scheme);

                // Log URL hình ảnh để kiểm tra
                Console.WriteLine("Image URL: " + imageUrl);

                orderDetailsHtml += $@"
                <tr>
                    <td>
                        <div class='product-details'>
                            <img src='{imageUrl}' alt='{item.Name}' class='product-image' style='width:100px;height:auto;'/>
                            <p class='product-name'>{item.Name}</p>
                        </div>
                    </td>
                    <td>{item.Quantity}</td>
                    <td>{String.Format("{0:N0}", item.Price)} đ</td>
                    <td>{String.Format("{0:N0}", item.Quantity * item.Price)} đ</td>
                </tr>";
            }

            // Điền dữ liệu vào mẫu HTML
            var emailBody = emailTemplate
                .Replace("{{ShippingAddress}}", order.ShippingAddress)
                .Replace("{{OrderDate}}", order.OrderDate.ToString("dd/MM/yyyy"))
                .Replace("{{TotalPrice}}", String.Format("{0:N0}", order.TotalPrice))
                .Replace("{{OrderNote}}", string.IsNullOrEmpty(order.Notes) ? "" : $@"
                <div class='summary-item'>
                    <h3>Ghi chú</h3>
                    <p>{order.Notes}</p>
                </div>")
                .Replace("{{OrderDetails}}", orderDetailsHtml);

            // Gửi email thông báo đơn hàng chi tiết
            var emailService = HttpContext.RequestServices.GetService<IEmailService>();
            var subject = "Đơn hàng của bạn đã được đặt thành công";
            await emailService.SendEmailAsync(user.Email, subject, emailBody);

            return View("OrderCompleted", order.Id);
        }

        public IActionResult PaymentCheckout()
        {
            return View();
        }

        private async Task<Product> GetProductFromDatabase(int productId)
        {
            return await _productRepository.GetByIdAsync(productId);
        }

        [HttpPost]
        public async Task<IActionResult> ProceedToMomoPayment(Order order)
        {
            var cart = HttpContext.Session.GetObjectFromJson<ShoppingCart>("Cart");
            if (cart == null || !cart.Items.Any())
            {
                return RedirectToAction("Index");
            }

            var user = await _userManager.GetUserAsync(User);
            order.UserId = user.Id;
            order.OrderDate = DateTime.UtcNow;
            order.TotalPrice = cart.Items.Sum(i => i.Price * i.Quantity);
            order.OrderDetails = cart.Items.Select(i => new OrderDetail
            {
                ProductId = i.ProductId,
                Quantity = i.Quantity,
                Price = i.Price
            }).ToList();

            if (string.IsNullOrEmpty(order.Notes))
            {
                order.Notes = "";
            }

            _context.Orders.Add(order);
            await _context.SaveChangesAsync();
            HttpContext.Session.Remove("Cart");

            var totalAmount = cart.Items.Sum(i => i.Price * i.Quantity);

            // Đọc nội dung từ file HTML
            var emailTemplate = System.IO.File.ReadAllText("C:\\Users\\acer\\Downloads\\DACS\\WebLKMT\\WebsiteBanHang\\WebsiteBanHang\\Views\\Email\\Email.cshtml");

            // Tạo nội dung chi tiết đơn hàng
            var orderDetailsHtml = "";
            foreach (var item in cart.Items)
            {
                // Đảm bảo URL hình ảnh là tuyệt đối
                var imageUrl = Url.Action("GetImage", "ShoppingCart", new { filename = item.Image }, Request.Scheme);

                // Log URL hình ảnh để kiểm tra
                Console.WriteLine("Image URL: " + imageUrl);

                orderDetailsHtml += $@"
                <tr>
                    <td>
                        <div class='product-details'>
                            <img src='{imageUrl}' alt='{item.Name}' class='product-image' style='width:100px;height:auto;'/>
                            <p class='product-name'>{item.Name}</p>
                        </div>
                    </td>
                    <td>{item.Quantity}</td>
                    <td>{String.Format("{0:N0}", item.Price)} VNĐ</td>
                    <td>{String.Format("{0:N0}", item.Quantity * item.Price)} VNĐ</td>
                </tr>";
            }

            // Điền dữ liệu vào mẫu HTML
            var emailBody = emailTemplate
                .Replace("{{ShippingAddress}}", order.ShippingAddress)
                .Replace("{{OrderDate}}", order.OrderDate.ToString("dd/MM/yyyy"))
                .Replace("{{TotalPrice}}", String.Format("{0:N0}", order.TotalPrice))
                .Replace("{{OrderNote}}", string.IsNullOrEmpty(order.Notes) ? "" : $@"
                <div class='summary-item'>
                    <h3>Ghi chú</h3>
                    <p>{order.Notes}</p>
                </div>")
                .Replace("{{OrderDetails}}", orderDetailsHtml);

            // Gửi email thông báo đơn hàng chi tiết
            var emailService = HttpContext.RequestServices.GetService<IEmailService>();
            var subject = "Đơn hàng của bạn đã được đặt thành công";
            await emailService.SendEmailAsync(user.Email, subject, emailBody);

            return RedirectToAction("Index", "Momo", new { amount = totalAmount });
        }

        public async Task<IActionResult> OrderHistory()
        {
            var user = await _userManager.GetUserAsync(User);
            var orders = await _context.Orders
                .Where(o => o.UserId == user.Id)
                .OrderByDescending(o => o.OrderDate)
                .ToListAsync();

            return View(orders);
        }

        public async Task<IActionResult> OrderDetails(int id)
        {
            var order = await _context.Orders
                .Include(o => o.OrderDetails)
                .ThenInclude(od => od.Product) // Bao gồm thông tin sản phẩm
                .FirstOrDefaultAsync(o => o.Id == id);

            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        [HttpGet]
        public IActionResult GetImage(string filename)
        {
            var imagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "images", filename);
            if (System.IO.File.Exists(imagePath))
            {
                var extension = Path.GetExtension(filename).ToLowerInvariant();
                var contentType = extension switch
                {
                    ".jpg" or ".jpeg" => "image/jpeg",
                    ".png" => "image/png",
                    ".gif" => "image/gif",
                    ".webp" => "image/webp",
                    _ => "application/octet-stream",
                };

                var image = System.IO.File.OpenRead(imagePath);
                return File(image, contentType);
            }
            return NotFound();
        }
    }
}
