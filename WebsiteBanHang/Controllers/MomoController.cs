﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebsiteBanHang.Models;
using WebsiteBanHang.Repositories;
using WebsiteBanHang.Services;

namespace WebsiteBanHang.Controllers
{
    [Authorize]
    public class MomoController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMomoService _momoService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IProductRepository _productRepository;

        public MomoController(IMomoService momoService, ApplicationDbContext context, UserManager<ApplicationUser> userManager, IProductRepository productRepository)
        {
            _momoService = momoService;
            _context = context;
            _userManager = userManager;
            _productRepository = productRepository;
        }

        public async Task<IActionResult> Index(decimal amount)
        {
            // Lấy ID của người dùng hiện tại
            var userId = _userManager.GetUserId(User);

            if (userId == null)
            {
                return Unauthorized(); // Hoặc phản hồi phù hợp khác
            }

            // Lấy thông tin người dùng từ UserManager
            var user = await _userManager.FindByIdAsync(userId);

            if (user == null)
            {
                return NotFound(); // Hoặc phản hồi phù hợp khác
            }

            // Lấy tên đầy đủ của người dùng
            var fullName = user.FullName; // Giả sử FullName là một thuộc tính của ApplicationUser

            // Truyền dữ liệu sang View
            ViewBag.FullName = fullName;
            ViewBag.TotalAmount = amount;

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreatePaymentUrl(OrderInfoModel model)
        {
            var response = await _momoService.CreatePaymentAsync(model);

            return Redirect(response.PayUrl);
        }

        [HttpGet]
        public IActionResult PaymentCallBack()
        {
            var response = _momoService.PaymentExecuteAsync(HttpContext.Request.Query);
            return View(response);
        }

    }
}
